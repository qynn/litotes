# Litotes

![cc-by-nc-sa-shield](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg)

Not unuseful 40% eronom-ish split mechanical keyboard.

- based on [XIAO RP2040](https://wiki.seeedstudio.com/XIAO-RP2040/)
- `2 x 6 x 4 = 48` switches (Cherry MX footprint)
- `148 x 86 mm` reversible PCB
- hand-solderable components only

![litotes-kle](doc/layout/litotes-kle.png)
